# Resume for Jorgen

1. Clone the repository
2. `yarn install --ignore-engines` some of the packages require engines that to not work with later than v1
3. `yarn serve`
4. Update `resume.json` file and see result in browser

- **Generate PDF:** `yarn export:pdf`
- **Generate HTML:** `yarn export:html`
